(function() {

	$("[data-action='add']").click(function() {
		var value = $(this).siblings("[data-role='value']")[0].value;
		var label = $(this).siblings("[data-role='label']")[0].value;
		if (label.length === 0) {
			label = value;
		}

		var listId = $(this).data('target');
		$('#' + listId).append($('<option>', {
			value : value,
			text : label
		}));
	});

	$("[data-role='value']").on('input propertychange paste', function(event) {
		handleValue($(this));
	});

	var handleValue = function(element) {
		var btn = $(element).siblings("[data-action='add']")[0];
		if ($(element).val().length === 0) {
			$(btn).attr('disabled', 'disabled');
		} else {
			$(btn).removeAttr('disabled');
		}
	}

	$(document).ready(function() {
		$("[data-role='value']").each(function() {
			handleValue($(this));
		});
	});
})();
