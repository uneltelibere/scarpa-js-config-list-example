# scarpa-js-config-list-example

Example for `scarpa-js-config-list`.

## How to try it

First run:

```
mvn clean install
```

Then open in your browser:

```
file:///path/to/scarpa-js-config-list-example/target/test-classes/index.html
```
